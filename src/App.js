import "./style/dark.scss";
import Home from "./pages/home/Home";
import Login from "./pages/login/Login";
import List from "./pages/list/List";
import Single from "./pages/single/Single";
import New from "./pages/new/New";
import { useContext } from "react";
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom";
import { userInputs } from "./formSource";
import { DarkModeContext } from "./context/darkModeContext";
import { AuthContext } from "./context/AuthContext";
import { choferColumns, jetsColumns, userColumns } from "./datatablesource";

function App() {
  const { darkMode } = useContext(DarkModeContext);

  const ProctectedRoutes = ({ children }) => {
    const { email } = useContext(AuthContext)

    if (!email) {
      return <Navigate to="/login" />
    }

    return children
  }

  return (
    <div className={darkMode ? "app dark" : "app"}>
      <BrowserRouter>
        <Routes>
          <Route path="/">
            <Route path="login" element={<Login />} />
            <Route
              index
              element={
                <ProctectedRoutes>
                  <Home />
                </ProctectedRoutes>
              }
            />
            <Route path="users">
              <Route index element={
                <ProctectedRoutes>
                  <List columns={userColumns} />
                </ProctectedRoutes>} />
              <Route path=":userId" element={
                <ProctectedRoutes>
                  <Single />
                </ProctectedRoutes>} />
              <Route
                path="new"
                element={
                  <ProctectedRoutes>
                    <New inputs={userInputs} title="Agregar nuevo usuario" />
                  </ProctectedRoutes>}
              />
            </Route>
            <Route path="jets">
              <Route
                index
                element={
                  <ProctectedRoutes>
                    <List columns={jetsColumns} />
                  </ProctectedRoutes>} />
              <Route path=":jetId" element={<Single />} />
            </Route>
            <Route path="choferes">
              <Route
                index
                element={
                  <ProctectedRoutes>
                    <List columns={choferColumns} />
                  </ProctectedRoutes>} />
              <Route path=":jetId" element={<Single />} />
            </Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
