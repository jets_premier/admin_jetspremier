import "./sidebar.scss";
import DashboardIcon from "@mui/icons-material/Dashboard";
import PersonOutlineIcon from "@mui/icons-material/PersonOutline";
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import LocalAirportIcon from '@mui/icons-material/LocalAirport';
import PeopleIcon from '@mui/icons-material/People';
import AirplaneTicketIcon from '@mui/icons-material/AirplaneTicket';
import { Link } from "react-router-dom";
import { DarkModeContext } from "../../context/darkModeContext";
import { useContext } from "react";

const Sidebar = () => {
  const { dispatch } = useContext(DarkModeContext);
  return (
    <div className="sidebar">
      <div className="top">
        <Link to="/" style={{ textDecoration: "none" }}>
          <span className="logo">Jets Premier</span>
        </Link>
      </div>
      <hr />
      <div className="center">
        <ul>
          <p className="title">MENÚ</p>
          <li>
            <DashboardIcon className="icon" />
            <span>Dashboard</span>
          </li>
          <p className="title">VER</p>
          <Link to="/users" style={{ textDecoration: "none" }}>
            <li>
              <PersonOutlineIcon className="icon" />
              <span>Usuarios</span>
            </li>
          </Link>
          <Link to="/jets" style={{ textDecoration: "none" }}>
            <li>
              <LocalAirportIcon className="icon" />
              <span>Jets</span>
            </li>
          </Link>
          <Link to="/choferes" style={{ textDecoration: "none"}}>
          <li>
            <PeopleIcon className="icon" />
            <span>Pilotos</span>
          </li>
          </Link>
          <p className="title">SERVICIOS</p>
          <li>
            <AirplaneTicketIcon className="icon" />
            <span>Vuelos vendidos</span>
          </li>
          <p className="title">USUARIO</p>
          <li>
            <AccountCircleOutlinedIcon className="icon" />
            <span>Mi Perfil</span>
          </li>
          <li>
            <ExitToAppIcon className="icon" />
            <span>Salir</span>
          </li>
        </ul>
      </div>
      <div className="bottom">
        <div
          className="colorOption"
          onClick={() => dispatch({ type: "LIGHT" })}
        ></div>
        <div
          className="colorOption"
          onClick={() => dispatch({ type: "DARK" })}
        ></div>
      </div>
    </div>
  );
};

export default Sidebar;
