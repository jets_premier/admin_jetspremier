export const userColumns = [
  { field: "_id", headerName: "ID", width: 70 },
  {
    field: "nombre",
    headerName: "Nombre",
    width: 230,
    renderCell: (params) => {
      return (
        <div className="cellWithImg">
          <img className="cellImg" src={params.row.img || "https://definicion.de/wp-content/uploads/2019/07/perfil-de-usuario.png"} alt="avatar" />
          {params.row.nombre}
        </div>
      );
    },
  },
  {
    field: "email",
    headerName: "Email",
    width: 230,
  },
  {
    field: "telefono",
    headerName: "Telefono",
    width: 200,
  },
  {
    field: "genero",
    headerName: "Genero",
    width: 100,
  },
];

export const jetsColumns = [
  { field: "_id", headerName: "ID", width: 100 },
  {
    field: "nombre",
    headerName: "Nombre",
    width: 150,
  },
  {
    field: "tipo",
    headerName: "Tipo",
    width: 100,
  },
  {
    field: "modelo",
    headerName: "Modelo",
    width: 200,
  },
  {
    field: "limite",
    headerName: "Limite",
    width: 150,
  },
  {
    field: "isAvailable",
    headerName: "Disponible",
    width: 100,
  },
];

export const choferColumns = [
  { field: "_id", headerName: "ID", width: 100 },
  {
    field: "nombre",
    headerName: "Nombre",
    width: 150,
  },
  {
    field: "edad",
    headerName: "Edad",
    width: 70,
  },
  {
    field: "licencia",
    headerName: "Licencia",
    width: 180,
  },
  {
    field: "email",
    headerName: "Email",
    width: 150,
  },
  {
    field: "telefono",
    headerName: "Telefono",
    width: 100,
  },
  {
    field: "createdAt",
    headerName: "Registrado",
    width: 150,
  },
];


