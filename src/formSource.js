export const userInputs = [
    {
      id: "email",
      label: "Email",
      type: "email",
      placeholder: "ejemplo@hotmail.com",
    },
    {
      id: "nombre",
      label: "Nombre",
      type: "text",
      placeholder: "Tu nombre completo",
    },
    {
      id: "telefono",
      label: "Telefono",
      type: "number",
      placeholder: "Tu numero de telefono",
    },
    {
      id: "password",
      label: "Password",
      type: "password"
    },
    {
      id: "genero",
      label: "Genero",
      type: "text",
      placeholder: "Genero"
    }
  ];
  
  export const productInputs = [
    {
      id: 1,
      label: "Title",
      type: "text",
      placeholder: "Apple Macbook Pro",
    },
    {
      id: 2,
      label: "Description",
      type: "text",
      placeholder: "Description",
    },
    {
      id: 3,
      label: "Category",
      type: "text",
      placeholder: "Computers",
    },
    {
      id: 4,
      label: "Price",
      type: "text",
      placeholder: "100",
    },
    {
      id: 5,
      label: "Stock",
      type: "text",
      placeholder: "in stock",
    },
  ];
  